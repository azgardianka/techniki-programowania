#include <iostream>

using namespace std;
int main() {
   
cout<<"Tabela stalych fizycznych\n";
cout<<"Nazwa\t Wartosc\t Jednostka\t Opis\n";
cout<< "c\t 2.99792e+008\t m/s\t         predkosc swiatla\n";
cout<< "G\t 6.67429e-11\t m^3/kg/s^2\t stala grawitacyjna\n";
cout<< "e\t 1.602176e-19\t C\t         ladunek elementarny\n";
cout<< "k\t 1.380649e-23\t J*s\t         stala Boltzmanna\n";
cout<< "ε0\t 8.854187e-12\t C^2/N*m^2\t przenikalnosc elektryczna\n";
cout<< "F\t 96485,339\t C/mol\t         stala Faradaya\n";
return 0;
}
