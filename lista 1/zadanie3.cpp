#include <iostream>
using namespace std;

int main()
{
    char znak;
    float cyfra1, cyfra2;

    cout << "Wprowadz znak +, -, * lub /: ";
    cin >> znak;

    cout << "Wprowadz dwa argumenty: ";
    cin >> cyfra1 >> cyfra2;

    switch(znak)
    {
        case '+':
            cout << cyfra1+cyfra2;
            break;

        case '-':
            cout << cyfra1-cyfra2;
            break;

        case '*':
            cout << cyfra1* cyfra2;
            break;

        case '/':
            cout << cyfra1/cyfra2;
            break;

        default:
            cout << "B��d znaku!";
            break;
    }

    return 0;}

