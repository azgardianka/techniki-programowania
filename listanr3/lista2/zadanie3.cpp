#include <iostream>
 
using namespace std;
 
 
int check(int n, int d) 
{
    if(n%d==0)
    {
        return d;
    }else
    {
        return 0;
    }
}
int main()
{
    int n;
    cout << "Podaj liczbe n >= 1: ";
    cin >> n;
 
    //inicjalizacja tablic
    int **dzielniki = new int*[n];
    for(int i = 0; i<n; i++)
    {
        dzielniki[i] = new int[i+1];
    }
 
    //wypelnienie tablic
    for(int i = 0; i<n; i++)
    {
        for(int d = 0; d < i+1; d++)
        {
            dzielniki[i][d] = check((i+1), (d+1));
        }
    }
 
    //wyoswietlanie tablic
    for(int i = 0; i<n; i++)
    {
        cout << endl << i+1 << "|";
        for(int d = 0; d < i+1; d++)
        {
           if(dzielniki[i][d] != 0)
            {
                cout << dzielniki[i][d] << " ";
            }
        }
    }
 
    //czyszczenie pamieci
    for(int i = 0; i<n; i++)
    {
        delete [] dzielniki[i];
    }
    delete [] dzielniki;
}
