#ifndef VECTORINT_HPP_INCLUDED
#define VECTORINT_HPP_INCLUDED

#include <iostream>

class VectorInt
{
    public:
    //konstruktor bez argumentow
    VectorInt();
    //konstruktor przyjmujacy jeden argument
    VectorInt(int);
    //konstruktor kopiujacy
    VectorInt(const VectorInt& other);
    //przeciazony operator przypisania
    VectorInt& operator= (const VectorInt& other);
    //destruktor
    ~VectorInt();
    //przyjmuje indeks i zwraca wartosc
    int at(int);
    //przyjmuje indeks i wartosc, ktora wstaiwa na dana pozycje
    void insert(int, int);
    //dodaje element na koncu ciagu, w razie potrzeby powiekszenie pamieci
    void pushBack (int);
    //usuwa ostatni element
    void popBack();
    //dostosowuje dlugosc wektora do wypelnionych komorek
    void shrinkToFit();
    //usuwa wszystkie elementy
    void clear();
    //zwraca liczbe elementow
    int size() const;
    //zwraca dlugosc zaalokowanej pamieci
    int capacity() const;
    //przeciozony operator wypisywania
    friend std::ostream& operator<< (std::ostream&, VectorInt&);
    void print();

    private:
        int* _arr;
        int _capacity;
        int _size;

};

#endif // VECTORINT_HPP_INCLUDED
