#include <iostream>
#include <string>

using namespace std;
bool pytajnik(const string& pattern, const string& s)
{
    int l_pattern = pattern.length();
    int l_s = s.length();
    if(l_pattern!=l_s) return false;
    else
    {
        for(int i=0;i<l_pattern;i++)
        {
            if(pattern[i]!='?')
            {
                if (pattern[i]!=s[i]) return false;
            }
        }
        return true;
    }
    return false;
}
bool match(const string& pattern, const string& s)
{
    int l_pattern = pattern.length();
    int l_s = s.length();
    string* pomoc, *pomoc2;
    int zapis=0,ilosc=0;
    pomoc=new string [0];
    for(int i=0;i<l_pattern;i++)
    {
        if (pattern[i]=='*')
        {
            ilosc++;
            pomoc2=new string[ilosc];
            for(int j=0;j<ilosc-1;j++)
            {
                pomoc2[j]=pomoc[j];
            }
            pomoc2[ilosc-1]=pattern.substr(zapis,i-zapis);
            pomoc=pomoc2;
            zapis=i+1;
        }
    }
    ilosc++;
    pomoc2=new string[ilosc];
    for(int j=0;j<ilosc-1;j++)
    {
        pomoc2[j]=pomoc[j];
    }
    pomoc2[ilosc-1]=pattern.substr(zapis,l_pattern-zapis);
    pomoc=pomoc2;
    zapis=0, ilosc=0;
    for(int i=0;i<l_pattern;i++)
    {
        if(pattern[i]=='*')
        {
           
            bool t=true;
            int a=pomoc[ilosc].length();
            for(int j=i+zapis+1;j<=(l_s-a);j++)
            {
                if(pytajnik(pomoc[ilosc],s.substr(j,a)))
                {
                    i=i+a;
                    ilosc++;
                    t=false;
                    break;
                }
                zapis++;
            }
            if(t)
            {
                return false;
            }
        }
        else
        {
            int a=pomoc[ilosc].length();
            if(pytajnik(pomoc[ilosc],s.substr(i+zapis,a)))
            {
                i=i+a-1;
                ilosc++;
            }
            else
            {
                return false;
            }
        }
    }
    int a=pomoc[ilosc-1].length();
    if(pytajnik(pomoc[ilosc-1],s.substr(l_s-a,a)))
    {
        return true;
    }
    return false;
}

int main()
{
    cout<<"Program sprawdzajacy zgodnosc tekstu z wzorcem"<<endl;
    cout<<"Wpisz wzorzec: ";
    string wzor, slowo;
    getline(cin,wzor);
    cout<<endl<<"Aby wyjsc nalezy wpisac \"koniec""\""<<endl;
    bool run=true;
    while(run)
    {
       cout<<"Wpisz slowo ktore chcesz porownac: ";
       getline(cin,slowo);
       if(slowo!="koniec")
       {
           if(match(wzor,slowo))
           {
               cout<<"Slowo pasuje do wzorca"<<endl;
           }
           else
           {
               cout<<"Slowo nie pasuje do wzorca "<<endl;
           }
       }
       else
       {
           return 1;
       }
    }


    return 0;
}

