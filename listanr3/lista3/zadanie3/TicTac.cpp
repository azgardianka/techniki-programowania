#include "tictac.hpp"
#include <iostream>

using namespace std;
TicTacToe::TicTacToe()
{
    _turn = KRZYZYK_P;

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            _board[i][j] = PUSTE;
        }
    }
}

gracz TicTacToe::whoseTurn()
{
    return _turn;
}

wynik TicTacToe::getPX()
{
    return _px;
}

wynik TicTacToe::getPY()
{
    return _py;
}

void TicTacToe::setTurn(int v)
{
    if (v%2 == 1)
    {
        _turn = KRZYZYK_P;

    }
    else
        _turn = KOLKO_P;

}

int TicTacToe::checkIfFull()
{

    int counter = 0;
    for (int j = 0; j < 3; j++)
    {
        for (int i = 0; i < 3; i++)
        {
            if (_board[i][j] != PUSTE)
                counter++;
        }
        if (counter == 9)
        {
            _px = REMIS;
            _py = REMIS;

            return 1;
        }
    }
}


void TicTacToe::placeMark(int& n, int& m)
{

    //dopuszczalnosc ruchu
    while ((n < 1 || n > 3) || (m < 1 || m > 3))
    {
        cout << "Podaj liczby z przedzialu od 1 do 3.\n";
        cin >> n;
        cin >> m;
    }


    while (_board[n-1][m-1]!=PUSTE)
    {
       cout << "Pole jest zajete. Wpisz nowe wspolrzedne.\n" << std::endl;
       cin >> n;
       cin >> m;
    }

    //ustawianie na planszy
    if (whoseTurn() == KRZYZYK_P)
    {
        _board[n-1][m-1] = KRZYZYK;

    } else
    {
        _board[n-1][m-1] = KOLKO;
    }
}

int TicTacToe::checkIfWon()
{

    pole mark;

    if (whoseTurn() == KRZYZYK_P)
    {
        mark = KRZYZYK;
    }
    else
        mark = KOLKO;

    int i, j, counter = 0;

    for (i = 0; i < 3; i++)
    {
        counter = 0;
        for (j = 0; j < 3; j++)
        {
            if (_board[i][j] == mark)
                counter++;
        }
        if (counter == 3)
        {
            if (whoseTurn() == KRZYZYK_P)
            {
                _px = WYGRANY;
                _py = PRZEGRANY;
            }
            else
            {
                _px = PRZEGRANY;
                _py = WYGRANY;
            }
            return 1;
        }
    }

    for (j = 0; j < 3; j++)
    {
        counter = 0;
        for (i = 0; i < 3; i++)
        {
            if (_board[i][j] == mark)
                counter++;
        }
        if (counter == 3)
        {
            if (whoseTurn() == KRZYZYK_P)
            {
                _px = WYGRANY;
                _py = PRZEGRANY;
            }
            else
            {
                _px = PRZEGRANY;
                _py = WYGRANY;
            }

            return 1;
        }
    }

    counter = 0;
    i = 1;
    j = 1;


    if (_board[i][j] == mark && ((_board[i-1][j-1] == mark && _board[i+1][j+1] == mark)|| (_board[i-1][j+1] == mark && _board[i+1][j-1] == mark)))
        counter = 3;

    if (counter == 3)
    {
        if (whoseTurn() == KRZYZYK_P)
        {
            _px = WYGRANY;
            _py = PRZEGRANY;
        }
        else
        {
            _px = PRZEGRANY;
            _py = WYGRANY;
        }
        return 1;
    }


}

//przeciazony operator<< dla wypisywania
ostream& operator<<(ostream& out, TicTacToe& a)
{
    cout << "\n";
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
          cout << a._board[i][j] << "|";
        }
        cout << "\n";
    }
    cout << "\n";
    return out;
}
