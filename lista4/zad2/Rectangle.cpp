#include "Rectangle.h"

//Konstruktor bez argumentów - kwadrat 1x1
Rectangle::Rectangle()
    : _a(1),
      _b(1)
{
}

// Konstruktor pobierający 2 boki
Rectangle::Rectangle(double a, double b)
    : _a(a > 0 ? a : throw std::domain_error("A musi byc  >0")),
      _b(b > 0 ? b : throw std::domain_error("B musi byc  >0"))
{
}

//Funkcja zwracająca pole
double Rectangle::area() const
{
    return this->_a * this->_b;
}

//Wypisanie
std::ostream &Rectangle::print(std::ostream &out) const
{
    out << "RECTANGLE z bokami: ("
        << this->_a << ", "
        << this->_b << ")\n";
    return out;
}
