#pragma once

#include <iostream>
#include <cmath>
#include "Figure.h"

class Triangle : public Figure
{
public:
    //Konstruktor bez argument�w - tr�jk�t o bokach 1,1,1
    Triangle();

    //Konstruktor pobieraj�cy 3 boki
    Triangle(double, double, double);

    //Destruktor domy�lny
    //~Triangle();

    //Funkcja zwracaj�ca pole
    double area() const;

    //Wypisanie
    std::ostream &print(std::ostream &) const;

private:
    //Bok a
    double _a;
    //Bok b
    double _b;
    //Bok c
    double _c;
};
