#pragma once

#include <iostream>

class Figure
{
public:
    virtual double area() const = 0;
    virtual std::ostream &print(std::ostream &) const = 0;
    friend std::ostream &operator<<(std::ostream &out, Figure &f);
};


