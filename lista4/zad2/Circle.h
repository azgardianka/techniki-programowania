#pragma once

#include <iostream>
#include "Figure.h"

class Circle : public Figure
{
public:
    //Konstruktor bazowy - promie� 1
    Circle();

    //Konstruktor pobieraj�cy promie�
    Circle(double);

    //Destruktor domy�lny
    // ~Circle();

    //Funkcja zwracaj�ca pole
    double area() const;

    //Wypisanie
    std::ostream &print(std::ostream &) const;

private:
    //Warto�� promienia
    double _r;
};
