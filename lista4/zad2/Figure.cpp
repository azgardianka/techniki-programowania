#include <iostream>
#include "Figure.h"


std::ostream &operator<<(std::ostream &out, Figure &f)
{
    f.print(out);
    return out;
}
