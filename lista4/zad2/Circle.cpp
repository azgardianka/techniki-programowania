#include "Circle.h"

#define M_PI 3.14159265358979323846 /* pi */

//Konstruktor bazowy - promie� 1
Circle::Circle() : _r(1){};

//Konstruktor pobieraj�cy promie�
Circle::Circle(double r) : _r(r >0 ? r : throw std::domain_error("R musi byc wieksze od 0!")){};

//Funkcja zwracaj�ca pole
double Circle::area() const
{
    return M_PI * this->_r * this->_r;
}

//Wypisanie
std::ostream &Circle::print(std::ostream &out) const
{
    out << "CIRCLE z promieniem : (" << this->_r << ")\n";
    return out;
}
