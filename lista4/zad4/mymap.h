#pragma once

#include <iostream>

template <class K, class V>
struct Node
{
public:
    Node *_left;
    Node *_right;
    K _key;
    V _val;
};

template <class K, class V>
class Mymap
{
public:
    //konstruktor tworz�cy root
    Mymap() : _root(NULL)
    {
    }

    //destruktor niszcz�cy ca�e drzewo
    ~Mymap()
    {
        this->clear();
    }

    //dodawanie nowego obiektu
    void insert(K k, V v)
    {
        Node<K, V> *newbie = new Node<K, V>;
        newbie->_key = k;
        newbie->_val = v;
        newbie->_left = NULL;
        newbie->_right = NULL;

        if (_root == NULL)
        {
            _root = newbie;
            return;
        }
        Node<K, V> *parent = NULL, *current = _root;
        while (current != NULL)
        {
            if (newbie->_key < current->_key)
            {
                parent = current;
                current = current->_left;
                continue;
            }
            else if (newbie->_key > current->_key)
            {
                parent = current;
                current = current->_right;
                continue;
            }
            else
            {
                current->_val = newbie->_val;
                return;
            }
        }

        if (newbie->_key < parent->_key)
        {
            parent->_left = newbie;
        }
        else if (newbie->_key > parent->_key)
        {
            parent->_right = newbie;
        }
        return;
    }
    //usuwanie elementu
    void erase(K k)
    {
        erase(k, _root);
    }

    // //wy�wietlanie mapy
    void print()
    {
        std::cout << "Wyswietlenie drzewa w rosnacej kolejnosci kluczy\n"
                  << "Klucz : Wartosc\n";
        print(_root);
    }

    // //usuni�cie ca�ego drzewa
    void clear()
    {
        clear(_root);
    }

private:
    Node<K, V> *_root;

    void print(Node<K, V> *leaf)
    {
        if (leaf != NULL)
        {
            print(leaf->_left);
            std::cout << leaf->_key << " : " << leaf->_val << "\n";
            print(leaf->_right);
        }
    }

    Node<K, V> *erase(K k, Node<K, V> *rt)
    {
        if (rt == NULL)
        {
            return rt;
        }

        if (k < rt->_key)
        {
            rt->_left = erase(k, rt->_left);
            return rt;
        }
        else if (k < rt->_key)
        {
            rt->_right = erase(k, rt->_right);
            return rt;
        }

        //usuni�cie roota
        if (rt->_left == NULL)
        {
            Node<K, V> *temp = rt->_right;
            delete rt;
            return temp;
        }
        else if (rt->_right == NULL)
        {
            Node<K, V> *temp = rt->_left;
            delete rt;
            return temp;
        }

        else
        {
            Node<K, V> *tempParent = rt, *temp = rt->_right;
            while (temp->_left != NULL)
            {
                tempParent = temp;
                temp = temp->_left;
            }

            if (tempParent != rt)
            {
                tempParent->_left = temp->_right;
            }
            else
            {
                tempParent->_right = temp->_right;
            }
            rt->_key = temp->_key;
            delete temp;
            return rt;
        }
    }

    void clear(Node<K, V> *leaf)
    {
        if (leaf != NULL)
        {
            clear(leaf->_left);
            clear(leaf->_right);
            delete leaf;
        }
    }
};
