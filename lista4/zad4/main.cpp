#include <iostream>
#include "mymap.h"

int main()
{
    Mymap<int, int> map;
    map.insert(1, 1);
    map.insert(20, 2);
    map.insert(-5, 0);
    map.insert(10, 10);
    map.insert(15, 15);
    map.insert(30, 30);
    map.print();
    map.insert(20, 40);
    map.erase(1);
    map.print();
    return 0;
}
