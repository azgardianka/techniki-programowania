#include <exception>
#include <string>

class VectorException : public std::exception {
public:
    VectorException (const std::string& msg): message(msg) {} // konstruktor, akceptuje wiadomosc
    ~VectorException() throw() {}
    std::string& what() { return message; } // zwraca wiadomosc
private:
    std::string message;                         // zapisuje komunikat o wyj�tku
};
