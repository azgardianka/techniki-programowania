#include "VectorInt.hpp"
#include "VectorException.hpp"

#include <iostream>
#include <exception>

//konstruktor bez argumentow
VectorInt::VectorInt()
{
    _arr = new int [16];
    _capacity = 16;
    _size = 0;
}
//konstruktor przyjmujacy jeden argument
VectorInt::VectorInt(int length)
{
   if (length > 0)
   {
        _arr = new int[length];
        _capacity = length;
        _size = 0;
   }else
   {
       throw new VectorException("Podana wartosc jest mniejsza lub rowna 0. Nie utworzono obiektu.");

   }
}
//zwraca liczbe elementow
int VectorInt::size() const
{
    return _size;
}
//zwraca dlugosc zaalokowanej pamieci
int VectorInt::capacity() const
{
    return _capacity;
}
//konstruktor kopiujacy
VectorInt::VectorInt(const VectorInt& other)
{
    _arr = new int [other.capacity()];
    _capacity = other.capacity();
    _size = other.size();

    for (int i = 0; i < size(); i++)
        {
            _arr[i] = other._arr[i];
        }

}
//destruktor
VectorInt::~VectorInt()
{
    delete [] _arr;
    _arr=NULL;
}
//przyjmuje indeks i zwraca wartosc
int VectorInt::at(int index)
{
    return _arr[index];
}
//dodaje element na koncu ciagu, w razie potrzeby powiekszenie pamieci
void VectorInt::pushBack(int newElement)
{
    if (size()==capacity())
    {
        int* temp = new int [capacity() + 1];


        for (int i = 0; i < size(); i++)
        {
            temp[i] = _arr[i];
        }

        delete [] _arr;
        _capacity += 1;
        _arr = temp;

    }

    _arr[size()] = newElement;
    _size++;
}
//przyjmuje indeks i wartosc, ktora wstaiwa na dana pozycje
void VectorInt::insert(int index, int value)
{
    if (index > size() || index < 0)
    {
        std::cout<<"Nie mozna wykonac operacji. Wartosc poza dlugoscia wektora."<<std::endl;

    }else
    {
         if (index==capacity())
            pushBack(value);
        else
            _arr[index] = value;
    }
}
//usuwa ostatni element
void VectorInt::popBack()
{
    _size--;
}
 //dostosowuje dlugosc wektora do wypelnionych komorek
void VectorInt::shrinkToFit()
{
    int* temp = new int [size()];

    for (int i = 0; i < size(); i++)
    {
        temp[i] = _arr[i];
    }

     delete [] _arr;
    _arr = NULL;
    _arr = new int [size()];

    for (int i=0; i< size(); i++)
    {
      _arr[i] = temp[i];

    }

    delete [] temp;
    temp = NULL;
    _capacity = size();

}
//usuwa wszystkie elementy
void VectorInt::clear()
{
    _size=0;
    delete [] _arr;
   _arr = NULL;
   _arr = new int [size()];
}

//przeciazony operator przypisania
VectorInt& VectorInt::operator= (const VectorInt& other)
{
    this->_arr = new int [other.capacity()];
    this->_capacity = other.capacity();
    this->_size = other.size();

    for (int i = 0; i < size(); i++)
        {
           this-> _arr[i] = other._arr[i];
        }
    return *this;
}
//przeciozony operator wypisywania
std::ostream& operator<<(std::ostream& out, VectorInt& myVector)
    {
        for (int i = 0; i < myVector.size(); i++)
        {
            std::cout << myVector._arr[i] << " ";
        }
        std::cout<<std::endl;
        return out;
    }
