#include <iostream>
#include <exception>

#include "VectorInt.hpp"
#include "VectorException.hpp"

int main()
{
	VectorInt myVector123;
    VectorInt myVector;
    myVector.pushBack(12);
    myVector.pushBack(8);
    myVector.pushBack(1);
    myVector.pushBack(102);
    myVector.insert(3,5);
    std::cout<<myVector;
    std::cout<<"size:\n";
    std::cout<<myVector.size()<<std::endl;
    std::cout<<"capacity:\n";
    std::cout<<myVector.capacity()<<std::endl;
    myVector.shrinkToFit();
    std::cout<<"capacity po shrinkToSize:\n";
    std::cout<<myVector.capacity()<<std::endl;
    std::cout<<"Wartosc pod indeksem 2:"<<std::endl;
    std::cout<<myVector.at(2)<<std::endl;

    std::cout<<"Tworzenie obiektu przez konstruktor z ujemnym argumenem:\n";
    try
    {
        VectorInt my2(-3);
        std::cout<<my2.capacity()<<std::endl;
    }
    catch(VectorException &e)
    {
        std::cout<<e.what()<<std::endl;
    }

    std::cout<<"Obiekt utworzony przez konstruktor kopiujacy.\n";
    VectorInt my3(myVector);
    std::cout<<my3;
    my3.popBack();
    std::cout<<"Po usnieciu ostatniego elementu:\n"<<my3;

    VectorInt my4;
    my4 = my3;
    std::cout<<"Przypisanie obiektowi innego obiektu poprzez przeciozony operator przypisania:\n"<<my4;

    myVector.~VectorInt();
    my3.~VectorInt();
    my4.~VectorInt();
    std::cout<<std::endl;
    return 0;
}
