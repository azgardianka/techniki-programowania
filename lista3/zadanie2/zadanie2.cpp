//Program demonstrujacy mozliwosci klasy Point2d.
#include <iostream>
#include "point2d.hpp"
#include <cmath>
#define M_PI 4*atan(1.)


using namespace std;

int main()
{
    //obiekt a utworzony z wykorzystaniem konstruktura domyslnego
    Point2d a;
    //do wyswietlania wykorzystujemy przeciazony operator << std::ostream& operator<<(std::ostream& out, Point2d& p);
    cout << "a = " << a << endl;
    //metoda setXY ustawia wspolrzedne punktu
    a.setXY(3, 4);
    cout << "a = " << a << endl;

    //obiekt b utworzony z wykorzystaniem konstruktora pobierajacego
    //dwie liczby - wspolrzedne punktu
    Point2d b(-2,-1);
    cout << "b = " << b << endl;

    //obiekt c utworzony z wykorzystaniem konstruktora kopiujacego
    Point2d c(b);
    cout << "c = " << c << endl;

    //wykorzystanie metod get do odczytania wspolrzednych punktu
    cout << "a.x = "   << a.getX() << endl;
    cout << "a.y = "   << a.getY() << endl;
    cout << "a.r = "   << a.getR() << endl;
    cout << "a.phi = " << a.getPhi() << endl;

    //metoda setRPhi ustawia wspolrzedne punktu na podstawie wspolrzednych biegunowych
    b.setRPhi(5,1);
    cout << "b = " << b << endl;
    Point2d e(a);
    cout<<e<<"  "<<e.getPhi();
    e.jednok(2);
    cout<<endl<<e;
    e.obrot(2*M_PI);
    cout<<endl<<e;


    return 0;
}

