#include "tictac.hpp"

#include <iostream>
using namespace std;

int main ()
{

    TicTacToe a;
    int n, m, i = 0, run = 0;


    while (run == 0)
    {
        cout << "Gracz " << a.whoseTurn() << "\nWpisz numer wiersza i kolumny.\n";
        cin >> n;
        cin >> m;
        a.placeMark(n, m);
        cout << a;
        if (a.checkIfWon() == 1)
            break;
        if (a.checkIfFull() == 1)
            break;
        a.setTurn(i);


        i++;
    }

    if (a.getPX() == WYGRANY)
    {
        cout << "Gracz " << KRZYZYK_P << " wygral." << endl;
    }
    else if (a.getPY() == WYGRANY)
    {
        cout << "Gracz " << KOLKO_P << " wygral." << endl;
    }
    else
    {
        cout << "Remis." << endl;
    }
    return 0;
}
