#ifndef TICTAC_HPP_INCLUDED
#define TICTAC_HPP_INCLUDED

#include <iostream>

enum pole
{
    KRZYZYK = 'X', KOLKO = 'O', PUSTE = ' '
};
enum gracz
{
    KRZYZYK_P = 1, KOLKO_P = 2
};
enum wynik
{
    WYGRANY = 1, PRZEGRANY = 2, REMIS = 0
};

class TicTacToe
{
    public:

        //konstruktor
        TicTacToe();

        //destruktor
        ~TicTacToe() = default;

        //metody
        friend std::ostream& operator<< (std::ostream&, TicTacToe&);
        gracz whoseTurn();
        void setTurn(int);
        void placeMark(int&, int&);
        int checkIfFull();
        int checkIfWon();
        wynik getPX();
        wynik getPY();

    private:
        char _board[3][3];
        gracz _turn;
        wynik _px;
        wynik _py;
};


#endif // TICTAC_HPP_INCLUDED
