#include <iostream>

using namespace std;

//tablica dwuwymiarowa
int t[3][3]= {{1,2,3},{4,5,6},{7,8,9}};


//czyszczenie planszy
void wyczysc (char t[3][3])
{
    int i,j;
    for (i=0;i<3;i++)
        for (j=0;j<3;j++)
            t[i][j]=' ';

}
//rysowania planszy do gry
void plansza(char t[])
{
  for(int i = 1; i <= 9; i++)
  {
    cout << " " << t[i] << " ";
    if(i % 3)
      cout << "|";
    else if(i != 9)
      cout << "\n---+---+---\n";
    else cout << endl;        
  }    
}

// zwracanie true, je�li nastapila wygrana danego zawodnika
bool wygrana(char t[], char g)
{
  bool test;
  int i;
  
  test = false;
  for(i = 1; i <= 7; i += 3)
    test |= ((t[i] == g) && (t[i+1] == g) && (t[i+2] == g));
  for(i = 1; i <= 3; i++)
    test |= ((t[i] == g) && (t[i+3] == g) && (t[i+6] == g));
    test |= ((t[1] == g) && (t[5] == g) && (t[9] == g));
    test |= ((t[3] == g) && (t[5] == g) && (t[7] == g));
  if(test)
  {
    plansza(t);
    cout << "\nGRACZ " << g << " WYGRYWA!!!\n\n";
    return true;
  }
  return false;
}

//zwracanie true, jesli na planszy nie ma juz zadnego wolnego pola na ruch
bool remis(char t[])
{
  for(int i = 1; i <= 9; i++)
    if(t[i] == ' ') return false;
  plansza(t);
  cout << "\nREMIS\n";
  return true;     
}

// mozliwosc ruchu gracza, po ruchu nastepuje zamiana gracza
void ruch(char t[], char &gracz)
{
   int r;
   int error=0;
   while(error==0) //sprawdzenie czy drugi gracz nie wybral tego samego pola
    {
        plansza(t);
        cout << "\nGRACZ " << gracz << " : Twoj ruch : ";
        cin >> r;
        cout << "-----------------------\n\n";
        if(((r >= 1) && (r <= 9)) && (t[r] == ' '))
        {
            t[r] = gracz;
            error=1;
        }
        else
        {
            cout << "Wybrales zla pozycje, sprobuj jeszcze raz!" << endl << endl;
        }
    }
    gracz = (gracz == 'O') ? 'X' : 'O';
}

main()
{
  char p[10];
  char g, w;
  
  do
  {
    cout << "Kolko i Krzyzyk\n";
    for(int i = 1; i <= 9; i++) p[i] = ' ';
    g = 'O';
    while(!wygrana(p,'X') && !wygrana(p,'O') && !remis(p)) ruch(p,g);
    cout << "Jeszcze raz ? (T = TAK) : ";
    cin >> w;
    cout << "\n\n\n";
  } while((w == 'T') || (w == 't'));
}
